package com.steelgate;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import com.restfb.DefaultJsonMapper;

public class LegacyJsonMapper extends DefaultJsonMapper {
	Logger _logger = Logger.getLogger(LegacyJsonMapper.class.getName());

   @Override
   public <T> T toJavaObject(String json, Class<T> type) {
       Object result = null;
       if (type == JSONObject.class) {
           try {
               result = new JSONObject(json);
           } catch (JSONException ex) {
        	   _logger.severe(ex.getMessage());
           }
           return (T) result;
       } else {
           return super.toJavaObject(json, type);
       }
   }

   @Override
   public <T> List<T> toJavaList(String json, Class<T> type) {
       try {
           return super.toJavaList(json, type);
       } catch (Exception ex) {
    	   _logger.severe(json);
       }
       return new ArrayList<>();
   }

   @Override
   public String toJson(Object object) {
//       return new JSONObject(object, names);
//       return "";
       return super.toJson(object);
   }
}