package com.steelgate;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.steelgate.GnipUtil;

public class TwitterSearcher {
	/* This is because the Gnip Powertrack rules state that we can only have 30 positive terms
	 * and we have to add "http://" to the end of the list.
	 */
	static final int DRUGS_PER_RULE = 28; 
	static int API_counter = 0;
	private final static List<String> _drugs;
	
	static {
		_drugs = HunterUtil.getDrugs(false, "Merck", "twitter");
		_drugs.addAll(HunterUtil.getDrugs(false, "Pfizer", "twitter"));
		_drugs.addAll(HunterUtil.getDevices());
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Date endDate = new Date();
		Calendar startTime = Calendar.getInstance();
		startTime.add(Calendar.DAY_OF_YEAR, -1);
		// build the rule here, 29 drugs + http:// at a time
		String rule = null;
		String[] tempDrugList = _drugs.toArray(new String[_drugs.size()]);
		int start = 0;
		int domainSeedsCounter = 0;
		while (start < tempDrugList.length) {
			StringBuilder ruleBuilder = new StringBuilder("(");
			int end = start + DRUGS_PER_RULE;
			if (end > tempDrugList.length) {
				end = tempDrugList.length;
			}
			for (int i = start;i < (start + DRUGS_PER_RULE) && (i < tempDrugList.length);i++) {
				String drug = tempDrugList[i];
				if (drug.contains(" ")) {
					drug = "\"" + drug + "\"";
				}
				ruleBuilder.append(drug);
				if (i < end - 1) {
					ruleBuilder.append(" OR ");
				}
			}
			ruleBuilder.append(") (\"http://\" OR \"https://\")");
			rule = ruleBuilder.toString();
			rule = rule.replace("\"", "\\\"");
			rule += " -is:retweet";
			System.err.println("rule is [" + rule + "]");
			start += DRUGS_PER_RULE;
//			long projectedResultCount = 0;
//			try {
//				projectedResultCount = GnipUtil.getNumberOfResultsForRule(rule, startTime.getTime(), endDate);
//				System.err.println("The rule [" + rule + "] is projected to contain " + projectedResultCount + " tweets.");
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
			JSONObject results = GnipUtil.search(rule, startTime.getTime(), endDate, false);
			if (results == null) {
				continue;
			}
//			System.err.println(results);
			JSONArray resultsArray = results.getJSONArray("results");
			for (int i = 0; i < resultsArray.length(); i++) {
				final JSONObject result = resultsArray.getJSONObject(i);
				final JSONObject gnip = result.getJSONObject("gnip");
				if (gnip.has("urls")) {
					final JSONArray urlArray = gnip.getJSONArray("urls");
					if (urlArray.length() > 0) {
						for (int j = 0;j < urlArray.length(); j++) {
							JSONObject url = urlArray.getJSONObject(j);
							String expandedURL = null;
							try {
								expandedURL = url.getString("expanded_url");
							} catch (Exception e) {
								System.err.println("problem getting expanded_url from this json: " + gnip);
								e.printStackTrace();
								continue;
							}
							try {
								String tweet = result.getString("body");
//								System.err.println("tweet: [" + tweet + "] expanded URL [" + expandedURL + "]");
								final JSONObject actor = result.getJSONObject("actor");
								String user = actor.getString("preferredUsername");
								String apiResponse = HunterUtil.saveDomainSeed(user + ": " + tweet, "Twitter", expandedURL);
//								System.err.println("API response: " + apiResponse);
								domainSeedsCounter++;
							} catch (JSONException e) {
								System.err.println("JSON exception while parsing: [" + result + "]");
								e.printStackTrace();
							}
						}
					}
				}
			}
			API_counter = GnipUtil.getAPIRequestCounter();
			System.err.println("we've made " + API_counter + " API requests to Gnip");
		}
		System.err.println("Sent " + domainSeedsCounter + " domains to the seeds API.");
	}
	
	public void finalize() {
		System.err.println("*** finalized!");
		System.err.println("we've made " + GnipUtil.getAPIRequestCounter() + " API requests to Gnip");
	}
	
}