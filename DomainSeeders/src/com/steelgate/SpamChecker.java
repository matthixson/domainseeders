package com.steelgate;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;

/**
 * This class checks the email account email.spam@steelgateis.com and tosses everything it finds there
 * into our SpamSearcherServlet running on cis-lookup.cloudapp.net. This was originally created as a
 * test of the Project Honey Pot spam feed, but we found it to be too low in quantity and quality of
 * OLP links.
 * 
 * 
 * @author mhixson
 *
 */
public class SpamChecker {
	public static final String HOSTNAME = "cis-lookup.cloudapp.net";
	public static final int PORT = 8080;
	static final Logger _logger = Logger.getLogger(SpamChecker.class.getName());
//	private static boolean textIsHtml = false;
	
	public static void main(String[] args) {
		System.err.println("Running at " + new Date());
		Properties props = new Properties();
		props.setProperty("mail.store.protocol", "imaps");
		try {
			Session session = Session.getInstance(props, null);
			Store store = session.getStore();
			store.connect("outlook.office365.com", "email.spam@steelgateis.com", "28ah2ln109HHkqnq"); // WTF? Took several minutes for the password change to be reflected for IMAP access.  old: Dago6841
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_WRITE);
			int unread = inbox.getUnreadMessageCount();
			if (unread < 1) {
				System.err.println("No unread messages.");
			} else {
				System.err.println(unread + " unread messages.");
			}
			for (int i = 0; i < unread; i++) {
				System.err.println("Processing message " + i);
				Message msg = inbox.getMessage(i + 1);
				Address[] in = msg.getFrom();
				try {
					String messageAsString;
					try {
						System.out.println("SENT DATE:" + msg.getSentDate());
						System.out.println("SUBJECT:" + msg.getSubject());
//						System.out.println("CONTENT:" + msg.getContent());
						for (Address address : in) {
							System.out.println("FROM:" + address.toString());
						}
						messageAsString = getText(msg);
						System.err.println("message: " + messageAsString);
					} catch (Exception e) {
						System.err.println("Problem parsing email. Deleting it.");
						e.printStackTrace();
						msg.setFlag(Flags.Flag.DELETED, true);
						continue;
					}
					_logger.info("opening a new socket to " + HOSTNAME + ":" + PORT);
					HttpURLConnection connection = null;
					URL API_URL = new URL("http://" + HOSTNAME + ":" + PORT + "/drugs/spamSearcher");
					connection = (HttpURLConnection) API_URL.openConnection();
					connection.setConnectTimeout(1000 * 10);
					connection.setRequestMethod("POST");
		      connection.setRequestProperty("Content-Type", "multipart/form-data");
		      connection.setRequestProperty("Content-Length", Integer.toString(messageAsString.getBytes().length));
		      connection.setRequestProperty("Content-Language", "en-US");  
		      connection.setUseCaches(false);
		      connection.setDoInput(true);
		      connection.setDoOutput(true);
					OutputStream output = connection.getOutputStream();
					output.write(messageAsString.getBytes());
					output.flush();
					output.close();
					_logger.info("waiting to read response code...");
					int responseCode = connection.getResponseCode();
					_logger.info("response code was [" + responseCode + "]");
				} catch (IOException e) {
					_logger.info("Problem communicating with SpamSearcher at " + HOSTNAME + ":" + PORT);
					e.printStackTrace();
				}
//				msg.setFlag(Flags.Flag.SEEN, true);
//	TODO: delete messages once we send them to SpamSearcherServlet			
				msg.setFlag(Flags.Flag.DELETED, true);
			}
			inbox.close(true);
		} catch (Exception texMex) {
			texMex.printStackTrace();
		}
	}
	
	/**
   * Return the primary text content of the message.
   */
	private static String getText(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			String s = (String) p.getContent();
//			textIsHtml = p.isMimeType("text/html");
			return s;
		}
		if (p.isMimeType("multipart/alternative")) {
			// prefer html text over plain text
			Multipart mp = (Multipart) p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
				Part bp = mp.getBodyPart(i);
				if (bp.isMimeType("text/plain")) {
					if (text == null) {
						text = getText(bp);
					}
					continue;
				} else if (bp.isMimeType("text/html")) {
					String s = getText(bp);
					if (s != null) {
						return s;
					}
				} else {
					return getText(bp);
				}
			}
			return text;
		} else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart) p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getText(mp.getBodyPart(i));
				if (s != null) {
					return s;
				}
			}
		}
		return null;
  }
}