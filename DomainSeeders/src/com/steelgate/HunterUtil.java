package com.steelgate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

public class HunterUtil {
	static final Logger _logger = Logger.getLogger(HunterUtil.class.getName());
//	static List<String> _blacklistedDomains = new ArrayList<String>();
	static final String DOMAIN_SEED_API = "http://cis-lookup.cloudapp.net:8081/api/seed";
//	static final String DOMAIN_SEED_API = "http://localhost:8081/api/seed";
	static final String DRUGS_API = "http://cis-lookup.cloudapp.net:8080/drugs/getDrugs?v=<include variations>&s=<source>"; //   "http://ceiba.matthixson.com:8181/Pulse/drugs?v=<include variations>&s=<source>";
	static final String DEVICES_API = "http://cis-lookup.cloudapp.net:8080/drugs/medicalDevices";
	static final Pattern domainPattern = Pattern.compile("^.*\\.([^\\.]+\\.[^\\.]+$)");
	
//	static {
//		SteelgateDatabaseClient sdc = SteelgateDatabaseClient.getInstance();
//		List<SearchTermFilter> terms = sdc.findAll(SearchTermFilter.class);
//		for (SearchTermFilter term : terms) {
//			switch (term.getType()) {
//				case 7:
//					_drugs.add(term.getTerm());
//					break;
//				case 4:
//					_blacklistedDomains.add(term.getTerm());
//					break;
//			}
//		}
//		_logger.info("loaded " + _blacklistedDomains.size() + " blacklisted domain names and " + _drugs.size() + " drugs");
//	}
	
	/**
	 * 
	 * @param includeVariations
	 * @param manufacturer
	 * @param source One of twitter, spam, indexed_web, facebook, or domain.
	 * @return
	 */
	public static List<String> getDrugs(boolean includeVariations, String manufacturer, String source) {
		List<String> drugList = new ArrayList<String>();
		// contact the drugs API
		try {
			String api = DRUGS_API.replace("<include variations>", (includeVariations) ? "true" : "false");
			api = api.replace("<source>", source);
			if (manufacturer != null) {
				api += "&m=" + manufacturer;
			}
			_logger.info("hitting API url [" + api + "]");
			URL API_URL = new URL(api);
			HttpURLConnection connection = (HttpURLConnection) API_URL.openConnection();
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(1000 * 10);
			connection.setDoOutput(true);
			StringBuilder sb = new StringBuilder();
			int responseCode = connection.getResponseCode();
			if (responseCode >= 200 && responseCode <= 299) {
				InputStream inputStream = connection.getInputStream();
				BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream));
					String line = null;
					do {
//							_logger.info("waiting to read...");
						line = bReader.readLine();
//							_logger.info("...line has been read: " + line);
						sb.append(line);
					} while (line != null);
			}
			JSONObject response = new JSONObject(sb.toString());
			_logger.info("response JSON is: " + response);
			JSONArray drugs = response.getJSONArray("drugs");
			for (int i = 0;i < drugs.length();i++) {
				JSONObject drug = drugs.getJSONObject(i);
				drugList.add(drug.getString("n"));
			}
		} catch (MalformedURLException raymond) {
			
		} catch (IOException e) {
			_logger.severe("Couldn't connect to the drugs API");
			e.printStackTrace();
		}
		Collections.sort(drugList);
		return(drugList);
	}
	
	public static List<String> getBlacklistedDomains() {
		return(new ArrayList<String>());
	}
	
	public static String saveDomainSeed(String description, String searchTool, String url) {
		String response = null;
		HttpURLConnection connection = null;
		boolean saved = false;
		while (!saved) {
			try {
				URL API_URL = new URL(DOMAIN_SEED_API);
				connection = (HttpURLConnection) API_URL.openConnection();
				connection.setRequestProperty("Content-Type", "application/json");
				connection.setConnectTimeout(1000 * 10);
				connection.setDoOutput(true);
				connection.setReadTimeout(5000);
	//			_logger.info("connection has been opened.");
				JSONObject query = new JSONObject();
				URL domainURL = new URL(url);
				Matcher matcher = domainPattern.matcher(domainURL.getHost());
				query.put("name", matcher.matches() ? matcher.group(1) : domainURL.getHost());
				query.put("description", description);
				query.put("searchTool", searchTool);
				query.put("url", url);
				OutputStream output = connection.getOutputStream();
	//			_logger.info("writing query to connection...");
				JSONArray array = new JSONArray();
				array.put(query);
	//			_logger.info("JSON query to seed api is: " + array.toString());
				output.write(array.toString().getBytes("UTF-8"));
				output.flush();
				output.close();
//				_logger.info("waiting to read response code...");
				int responseCode = connection.getResponseCode();
//				_logger.info("response was [" + responseCode + "]");
				if (responseCode >= 200 && responseCode <= 299) {
					InputStream inputStream = connection.getInputStream();
					BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream));
					String line = null;
					do {
	//					_logger.info("waiting to read...");
						line = bReader.readLine();
	//					_logger.info("...line has been read: " + line);
						if (line != null) {
							response = line;
						}
					} while (line != null);
					saved = true;
				}
			} catch (java.net.SocketTimeoutException seth) {
				System.err.println("**** read timed out from domain seeds API. trying again.");
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (connection != null) {
					connection.disconnect();
				}
			}
		}
		return(response);
	}

	public static List<String> getDevices() {
		List<String> drugList = new ArrayList<String>();
		// contact the devices API
		try {
			_logger.info("hitting API url [" + DEVICES_API + "]");
			URL API_URL = new URL(DEVICES_API);
			HttpURLConnection connection = (HttpURLConnection) API_URL.openConnection();
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(1000 * 10);
			connection.setDoOutput(true);
			StringBuilder sb = new StringBuilder();
			int responseCode = connection.getResponseCode();
			if (responseCode >= 200 && responseCode <= 299) {
				InputStream inputStream = connection.getInputStream();
				BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream));
					String line = null;
					do {
//							_logger.info("waiting to read...");
						line = bReader.readLine();
//							_logger.info("...line has been read: " + line);
						sb.append(line);
					} while (line != null);
			}
			JSONObject response = new JSONObject(sb.toString());
			_logger.info("response JSON is: " + response);
			JSONArray devices = response.getJSONArray("devices");
			for (int i = 0;i < devices.length();i++) {
				JSONObject drug = devices.getJSONObject(i);
				drugList.add(drug.getString("d"));
			}
		} catch (MalformedURLException raymond) {
			// The question, RAYMOND, is: What do you want to be?
		} catch (IOException e) {
			_logger.severe("Couldn't connect to the drugs API");
			e.printStackTrace();
		}
		Collections.sort(drugList);
		return(drugList);
	}
}
