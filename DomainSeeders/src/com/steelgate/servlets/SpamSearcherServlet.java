package com.steelgate.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.map.LRUMap;

import com.steelgate.HunterUtil;

/**
 * This class runs within Tomcat, serving as a daemon that accepts connections from SpamListener, which runs at Spam Arrest.
 * 
 * @author mhixson
 *
 */
public class SpamSearcherServlet extends HttpServlet {
	static final Logger _logger = Logger.getLogger(SpamSearcherServlet.class.getName());
	private final static List<String> _drugs;
	public static final int PORT = 8888;
	private static final String URL_REGEX = "((https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])";
	private static final Pattern pattern = Pattern.compile(URL_REGEX);
	private static HashSet<String> domainsAlreadyFound = new HashSet<String>();
//	private static HashSet<Integer> spamAlreadySeen = new HashSet<Integer>();
	private static long spamsWithDrugsCounter = 0;
	private static long urlCounter = 0;
	private int myNum = 0;
//	private static LRUCache<Integer, Boolean> spamAlreadySeen = new LRUCache<Integer, Boolean>(100000);
	private Map<Integer, Boolean> spamAlreadySeen = Collections.synchronizedMap(new LRUMap(100000));
	
	static {
		_drugs = HunterUtil.getDrugs(false, "Merck", "spam");
		_drugs.addAll(HunterUtil.getDrugs(false, "Pfizer", "spam"));
//		for (String drug : _drugs) {
//			Pattern drugPattern = Pattern.compile(".*\\b" + drug + "\\b.*", Pattern.CASE_INSENSITIVE);
//			drugPatterns.add(drugPattern);
//		}
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			String line = null;
			StringBuilder sb = new StringBuilder(2048);
			// only read one line at a time. incoming spam will have had newlines stripped
			BufferedReader in = new BufferedReader(request.getReader());
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
      final String message = sb.toString().replaceFirst("data=", "");
			if (spamAlreadySeen.keySet().contains(message.hashCode())) {
				// drop it on the floor
				//_logger.info("already have seen this spam [" + message + "]");
//				_logger.info("found a spam we've already seen out of our collection of " + spamAlreadySeen.size() + " spam emails");
				return;
			} else {
				spamAlreadySeen.put(message.hashCode(), new Boolean(true));
			}
			// look for drug names
			boolean drugFound = false;
			final String lowerCaseMessage = message.toLowerCase();
			for (String drug : _drugs) {
				if (lowerCaseMessage.contains(drug)) {
					drugFound = true;
					break;
				}
			}
			if (drugFound) {
				spamsWithDrugsCounter++;
				// show all URLs found
				Matcher matcher = pattern.matcher(message);
				while (matcher.find()) {
					String url = matcher.group(1);
					URL domainURL;
					try {
						domainURL = new URL(url);
					} catch (MalformedURLException e) {
						_logger.warning("*** invalid URL [" + url + "]");
						return;
					}
					String domain = domainURL.getHost();
					// clean up the domain
					if (domain.contains("=")) {
						String cleanDomain = domain;
						if (cleanDomain.contains("=2e")) {
							cleanDomain = cleanDomain.replaceAll("=2e", ".");
//							_logger.info("replaced one or more occurences of =2e in the domain to create [" + cleanDomain + "]");
						}
						// if there are any = left over replace them
						cleanDomain = cleanDomain.replaceAll("=", "");
//							_logger.info("deleted one or more occurences of = in the domain to create [" + cleanDomain + "]");
						URL cleanURL = new URL(domainURL.toString().replaceFirst(domain, cleanDomain));
//						_logger.info("cleaning URL from [" + url + "] to [" + cleanURL.toString() + "]");
						url = cleanURL.toString();
					}
					if (!domainsAlreadyFound.contains(domain)) {
//						_logger.info("found a URL with a new domain [" + url + "]");
						urlCounter++;
						HunterUtil.saveDomainSeed(message, "spam", url);
						domainsAlreadyFound.add(domain);
//						_logger.info(((float) urlCounter / (float) spamsWithDrugsCounter) + " new URLs found per drug spam.");
					}
				}
//				_logger.info("this email contained " + urlCounterPerEmail + " URLs.");
//				if ((spamCounter % 100) == 0) {
//					_logger.info(((float)spamCounter / (float)((System.currentTimeMillis() - startTime) / 1000)) + " spams per second");
//				}
			}
		} catch (IOException e) {
			_logger.info("problem reading query");
			e.printStackTrace();
		}
	}
	
	public int getMyNum() {
		return myNum;
	}

	public void setMyNum(int myNum) {
		this.myNum = myNum;
	}
}