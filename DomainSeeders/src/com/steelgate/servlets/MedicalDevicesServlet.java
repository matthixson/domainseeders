package com.steelgate.servlets;

import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

public class MedicalDevicesServlet extends HttpServlet {
	
static final Logger _logger = Logger.getLogger(DrugServlet.class.getName());
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		JSONObject json = new JSONObject();
		JSONArray devicesArray = new JSONArray();
		JSONObject deviceJson = new JSONObject();
		deviceJson.put("d", "one touch ultra");
		deviceJson.put("m", "LifeScan, Inc.");
		devicesArray.put(deviceJson);
		json.put("devices", devicesArray);
		response.getOutputStream().write(json.toString().getBytes());
	}
	
}
