package com.steelgate.servlets;

import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.tools.view.WebappResourceLoader;

public class TemplateManager implements ServletContextListener {
	static VelocityEngine _ve;
	
	public static final Template getTemplate(String templateFile) {
		return(_ve.getTemplate(templateFile));
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		Properties prop = new Properties();
        prop.setProperty("resource.loader", "webapp");
        prop.setProperty("webapp.resource.loader.class", WebappResourceLoader.class.getName());
        prop.setProperty("webapp.resource.loader.path", "/WEB-INF/templates/");
        _ve = new VelocityEngine();
        _ve.setApplicationAttribute("javax.servlet.ServletContext", event.getServletContext());
        _ve.init(prop);
	}
	
}
