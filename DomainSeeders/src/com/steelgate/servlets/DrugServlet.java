package com.steelgate.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.steelgate.persistence.Drug;
import com.steelgate.persistence.Manufacturer;
import com.steelgate.persistence.SteelgateDatabaseClient;
import com.steelgate.persistence.Variation;

/** 
 * This servlet is meant to server JSON data to calling software. For the servlet that serves
 * things in human readable form see DrugBrowserServlet.
 * 
 * The output of this servlet is documented here:
 * 
 * https://ceibaintelligence.atlassian.net/wiki/display/STEEL/Drug+Names+API
 * 
 * @author mhixson
 *
 */
public class DrugServlet extends HttpServlet {
  static final Logger _logger = Logger.getLogger(DrugServlet.class.getName());
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		SteelgateDatabaseClient sdc = SteelgateDatabaseClient.getInstance();
		Collection<Manufacturer> manufacturers = null;
		String mFromQuery = request.getParameter("m");
		String sourceFromQuery = request.getParameter("s");
		JSONObject json = new JSONObject();
		if (mFromQuery != null) {
			Manufacturer manuf;
			try {
				manuf = sdc.findManufacturerByName(mFromQuery);
			} catch (javax.persistence.NoResultException ned) {
				JSONObject error = new JSONObject();
				error.put("error", "Manufacturer [" + mFromQuery + "] does not exist.");
				response.getOutputStream().write(error.toString().getBytes());
				return;
			}
			manufacturers = new ArrayList<Manufacturer>();
			manufacturers.add(manuf);
		} else {
			manufacturers = sdc.findAll(Manufacturer.class);
		}
		JSONArray manufArray = new JSONArray();
		JSONArray drugsArray = new JSONArray();
		String vParam = request.getParameter("v");
		boolean includeVariations = true;
		if (vParam != null && vParam.equals("false")) {
			includeVariations = false;
		}
		HashSet<String> drugsIncludedInOutput = new HashSet<String>();
		if (sourceFromQuery != null) {
			sourceFromQuery = sourceFromQuery.toLowerCase();
		}
		for (Manufacturer m : manufacturers) {
			JSONObject manufJson = new JSONObject();
			manufJson.put("id", m.getId());
			manufJson.put("name", m.getName());
			manufArray.put(manufJson);
			for (Drug drug : sdc.getDrugsForManufacturer(m)) {
				if (sourceFromQuery != null) {
					boolean include = false;
					switch (sourceFromQuery) {
						case "spam":
							if (drug.isSpam()) {
								include = true;
							}
							break;
						case "facebook":
							if (drug.isFacebook()) {
								include = true;
							}
							break;
						case "twitter":
//							_logger.info("drug [" + drug.getName() + "] has a twitter value of [" + drug.isTwitter() + "]");
							if (drug.isTwitter()) {
								include = true;
							}
							break;
						case "indexed_web":
							if (drug.isIndexedWeb()) {
								include = true;
							}
							break;
						case "domain":
							if (drug.isDomain()) {
								include = true;
							}
							break;
					}
					if (!include) {
						continue;
					}
				}
				JSONObject drugJson = new JSONObject();
				drugJson.put("n", drug.getName());
				drugJson.put("m", m.getId());
				if (includeVariations) {
					Collection<Variation> variations = drug.getVariations();
					if (variations != null && variations.size() > 0) {
						JSONArray variationsArray = new JSONArray();
						for (Variation v : variations) {
							JSONObject variationJson = new JSONObject();
							variationJson.put("v", v.getValue());
							variationJson.put("m", m.getId());
							variationsArray.put(variationJson);
						}
						drugJson.put("variations", variationsArray);
					}
				} else {
					if (drugsIncludedInOutput.contains(drug.getName())) {
						continue;
					}
				}
				drugsIncludedInOutput.add(drug.getName());
				drugsArray.put(drugJson);
			}
		}
		json.put("manufacturers", manufArray);
		json.put("drugs", drugsArray);
		response.getOutputStream().write(json.toString().getBytes());
	}
	
}
