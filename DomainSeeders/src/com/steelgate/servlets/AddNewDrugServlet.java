package com.steelgate.servlets;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.VelocityContext;

import com.steelgate.persistence.Drug;
import com.steelgate.persistence.Manufacturer;
import com.steelgate.persistence.SteelgateDatabaseClient;
import com.steelgate.persistence.Variation;

public class AddNewDrugServlet extends HttpServlet {
	static final Logger _logger = Logger.getLogger(AddNewDrugServlet.class.getName());
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		_logger.info("\nI want a new drug\nOne that won't make me sick\nOne that won't make me crash my car\nOr make me feel three feet thick");
		final String drugName = request.getParameter("name").toLowerCase();
		_logger.info("creating a new drug named [" + drugName + "]");
		// see if this name exists already and display an error if so
		SteelgateDatabaseClient sdc = SteelgateDatabaseClient.getInstance();
		final VelocityContext context = new VelocityContext();
		Drug drug = sdc.findDrugByName(drugName);
		if (drug != null) {
			context.put("error", "The drug [" + drugName + "] already exists: " + drug);
		} else {
			// save drug
			sdc.beginTransaction();
			drug = new Drug();
			drug.setName(drugName);
		  Variation v = new Variation();
		  v.setValue(drugName);
		  Manufacturer m = sdc.findSingleResult(Manufacturer.class, Long.parseLong(request.getParameter("manufacturerId")));
		  ArrayList<Variation> variations = new ArrayList<Variation>();
		  v.setManufacturer(m);
		  variations.add(v);
		  drug.setVariations(variations);
		  drug.setSpam("on".equalsIgnoreCase(request.getParameter("spam")));
		  drug.setFacebook("on".equalsIgnoreCase(request.getParameter("facebook")));
		  drug.setDomain("on".equalsIgnoreCase(request.getParameter("domain")));
		  drug.setTwitter("on".equalsIgnoreCase(request.getParameter("twitter")));
		  drug.setIndexedWeb("on".equalsIgnoreCase(request.getParameter("indexedWeb")));
		  sdc.merge(drug);
		  sdc.endTransaction();
		  context.put("success", "Drug [" + drugName + "] has been saved.");
		}
		List<Manufacturer> manufacturers = sdc.findAll(Manufacturer.class);
		Collections.sort(manufacturers);
		final StringWriter writer = new StringWriter();
		context.put("manufacturers", manufacturers);
		TemplateManager.getTemplate("addNewDrug.vm").merge(context, writer);
		response.getOutputStream().write(writer.toString().getBytes());
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		SteelgateDatabaseClient sdc = SteelgateDatabaseClient.getInstance();
		List<Manufacturer> manufacturers = sdc.findAll(Manufacturer.class);
		Collections.sort(manufacturers);
		final VelocityContext context = new VelocityContext();
		final StringWriter writer = new StringWriter();
		context.put("manufacturers", manufacturers);
		TemplateManager.getTemplate("addNewDrug.vm").merge(context, writer);
		response.getOutputStream().write(writer.toString().getBytes());
	}
	
}
