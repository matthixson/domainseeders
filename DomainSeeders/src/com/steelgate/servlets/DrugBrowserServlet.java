package com.steelgate.servlets;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.VelocityContext;

import com.steelgate.persistence.Drug;
import com.steelgate.persistence.Manufacturer;
import com.steelgate.persistence.SteelgateDatabaseClient;

public class DrugBrowserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger _logger = Logger.getLogger(DrugBrowserServlet.class.getName());
	static final StringBuilder alphabet;
	
	static {
		alphabet = new StringBuilder();
		alphabet.append("[ ");
		for (char i = 'a';i <= 'z';i++) {
			alphabet.append("<a href=\"drugBrowser?startsWith=" + i + "\"> " + i + " </a>");
			if (i < 'z') {
				alphabet.append("-");
			}
		}
		alphabet.append(" ]");
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		final String startsWith = request.getParameter("startsWith");
		SteelgateDatabaseClient sdc = SteelgateDatabaseClient.getInstance();
		List<Drug> drugs = null;
		final VelocityContext context = new VelocityContext();
		if (startsWith != null) {
			context.put("startsWith", startsWith);
			drugs = sdc.findDrugsStartingWith(startsWith);
		} else {
			String source = request.getParameter("source");
			if (source != null) {
				drugs = sdc.findAll(Drug.class);
				context.put("source", source);
				drugs = sdc.findDrugsForSource(source.toLowerCase());
			} else {
				String manufacturerId = request.getParameter("m");
				if (manufacturerId != null) {
					Manufacturer m = sdc.findSingleResult(Manufacturer.class, Long.parseLong(manufacturerId));
					drugs = sdc.getDrugsForManufacturer(m);
					context.put("manufacturer", m);
				}
			}
		}
		final StringWriter writer = new StringWriter();
		context.put("drugs", drugs);
		context.put("alphabet", alphabet);
		// get all drugs and count them
		List<Drug> allDrugs = sdc.findAll(Drug.class);
		context.put("numberOfDrugs", allDrugs.size());
		// get all variations and count them
		long variations = 0;
		for (Drug d : allDrugs) {
			variations += d.getVariations().size();
		}
		context.put("numberOfVariations", variations);
		List<Manufacturer> manufacturers = sdc.findAll(Manufacturer.class);
		Collections.sort(manufacturers);
		context.put("manufacturers", manufacturers);
		TemplateManager.getTemplate("drugBrowser.vm").merge(context, writer);
		response.getOutputStream().write(writer.toString().getBytes());
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, String[]> params = request.getParameterMap();
		SteelgateDatabaseClient sdc = SteelgateDatabaseClient.getInstance();
		for (String paramName : params.keySet()) {
			if (paramName.equals("drug")) {
//				_logger.info("parameter [" + paramName + "] has value(s):");
				sdc.beginTransaction();
				for (String drugName : params.get("drug")) {
					boolean spamChecked = formValueChecked(request, drugName + "_spam");
					boolean twitterChecked = formValueChecked(request, drugName + "_twitter");
					boolean domainChecked = formValueChecked(request, drugName + "_domain");
					boolean facebookChecked = formValueChecked(request, drugName + "_facebook");
					boolean indexedWebChecked = formValueChecked(request, drugName + "_indexedWeb");
//					_logger.info("drug [" + drugName + "] spam is " + ((spamChecked) ? "checked" : "unchecked"));
					Drug drug = sdc.findDrugByName(drugName);
					if (drug == null) {
						// skip it.
						continue;
					}
					boolean modified = false;
					if (spamChecked != drug.isSpam()) {
						drug.setSpam(spamChecked);
						modified = true;
					}
					if (twitterChecked != drug.isTwitter()) {
						drug.setTwitter(twitterChecked);
						modified = true;
					}
					if (domainChecked != drug.isDomain()) {
						drug.setDomain(domainChecked);
						modified = true;
					}
					if (facebookChecked != drug.isFacebook()) {
						drug.setFacebook(facebookChecked);
						modified = true;
					}
					if (indexedWebChecked != drug.isIndexedWeb()) {
						drug.setIndexedWeb(indexedWebChecked);
						modified = true;
					}
					if (modified) {
						sdc.merge(drug);
					}
				}
				sdc.endTransaction();
			}
		}
		// and then just re-render the page as if this was a get
		this.doGet(request, response);
	}
	
	private boolean formValueChecked(HttpServletRequest request, String paramName) {
		return("on".equals(request.getParameter(paramName)));
	}
	
}
