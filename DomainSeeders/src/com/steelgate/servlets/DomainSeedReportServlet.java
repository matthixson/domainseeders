package com.steelgate.servlets;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

import com.steelgate.persistence.DomainSeed;
import com.steelgate.persistence.SteelgateDatabaseClient;

/**
 * This servlet reads the domain seeds database and displays stats based upon the seed source (domain_seeds.search_tool).
 * @author mhixson
 *
 */
public class DomainSeedReportServlet extends HttpServlet {
	static final Logger _logger = Logger.getLogger(DomainSeedReportServlet.class.getName());
	private TemplateManager _templateManager = new TemplateManager();
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		final Date startDate = parseDate(request.getParameter("startDate"));
		final Date endDate = parseDate(request.getParameter("endDate"));
		_logger.info("start date is [" + startDate + "] and endDate is [" + endDate + "]");
		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DAY_OF_YEAR, -1);
		SteelgateDatabaseClient sdc = SteelgateDatabaseClient.getInstance();
//		List<DomainSeed> seeds = sdc.findDomainSeedsBetweenDates(startDate, endDate);
//		HashMap<String, List<DomainSeed>> domainsBySearchTool = new HashMap<String, List<DomainSeed>>();
//		for (DomainSeed domain : seeds) {
////			_logger.info("loaded domain [" + domain.getName() + "]");
//			if (!domainsBySearchTool.containsKey(domain.getSearchTool())) {
//				domainsBySearchTool.put(domain.getSearchTool(), new ArrayList<DomainSeed>());
//			}
//			List<DomainSeed> seedList = domainsBySearchTool.get(domain.getSearchTool());
//			seedList.add(domain);
//			domainsBySearchTool.put(domain.getSearchTool(), seedList);
//		}
		StringBuilder sb = new StringBuilder();
		sb.append("<html><h3>Counts between " + startDate + " and " + endDate + "<table>");
		Map<String, Long> resultMap = sdc.getDomainSourcesStatsBetweenDates(startDate, endDate);
		long total = 0;
		for (String searchTool : resultMap.keySet()) {
			long count = resultMap.get(searchTool);
			sb.append("<tr><td>" + searchTool + "</td><td>" + count + "</td></tr>");
			total += count;
		}
		sb.append("<tr><td>Total</td><td>" + total + "</td></tr>");
		sb.append("</table></html>");
		response.getOutputStream().write(sb.toString().getBytes());
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Template template = _templateManager.getTemplate("domainSeedReportForm.vm");
		_logger.info("the template is " + template);
		final VelocityContext context = new VelocityContext();
		final StringWriter writer = new StringWriter();
//		writer.append("this came from the StringWriter");
		template.merge(context, writer);
		response.getOutputStream().write(writer.toString().getBytes());
	}

	private Date parseDate(final String dateString) {
		Calendar cal = Calendar.getInstance();
		String[] dateArray = dateString.split("/");
		cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateArray[1]));
		cal.set(Calendar.MONTH, Integer.parseInt(dateArray[0]) - 1); // the
																	 // Calendar.MONTH
																	 // is indexed
																	 // from 0
		cal.set(Calendar.YEAR, Integer.parseInt(dateArray[2]));
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.AM_PM, Calendar.AM);
		return(cal.getTime());
	}
}
