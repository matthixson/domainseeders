package com.steelgate;

import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.DefaultWebRequestor;
import com.restfb.FacebookClient;
import com.restfb.Parameter;

/**
 * This class searches Facebook
 * @author mhixson
 *
 */
public class FacebookSearcher {
	private final static List<String> _drugs;	
	
	static {
		_drugs = HunterUtil.getDrugs(false, "Merck", "facebook");
		_drugs.addAll(HunterUtil.getDrugs(false, "Pfizer", "facebook"));
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.err.println("There are " + _drugs.size() + " drugs in our list.");
		DefaultWebRequestor web = new DefaultWebRequestor();
		// this token is our appId + | + app secret.
		FacebookClient client = new DefaultFacebookClient("249547905225033|e5331b6f3d73717ad55b5cf9285beb5f", web, new LegacyJsonMapper());
		int linkCounter = 0;
		for (String drug : _drugs) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Connection postsSearch = client.fetchConnection("search",
	        JSONObject.class, Parameter.with("q", drug),
	//        Parameter.with("type", "post"),
	        Parameter.with("fields", "id, name, link, description"),
	        Parameter.with("limit", 1000),
	        Parameter.with("offset", 0));
			for (Object obj : postsSearch.getData()) {
			    JSONObject json = (JSONObject) obj;
	//		    System.err.println("json is [" + json + "]");
	//		    System.err.println("group: " + group.getString("name"));
	//		    if (group.has("description")) {
	//		        System.err.println("description: " + group.getString("description"));
	//		    }
			    if (json.has("link") && (json.has("description") || json.has("name"))) {
			    	String link = json.getString("link");
			    	String text = null;
						try {
							text = json.getString("description");
						} catch (JSONException e) {
							try {
								text = json.getString("name");
							} catch (JSONException e1) {
								System.err.println("****** this json didn't have a description nor a name: " + json);
							}
						}
			    	if (!link.contains("facebook") && !link.contains("viral-wtf-videos")) {
			    		System.err.println("found [" + drug + "] in json: " + json);
			    		HunterUtil.saveDomainSeed(text, "Facebook", link);
			    		linkCounter++;
			    	}
			    }
			}
		}
		System.err.println(linkCounter + " links found");
//		System.err.println("The next page is: " + postsSearch.getNextPageUrl());
	}
}
