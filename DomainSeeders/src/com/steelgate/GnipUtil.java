package com.steelgate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import redis.clients.jedis.Jedis;

/** Say it with me, "Guh-nip-yoo-till." */
public final class GnipUtil {
	static final Jedis jedis = new Jedis("localhost");
	private static final String basicAuthToken = "Basic " + Base64.encodeBase64String("mhixson@iswarm.com:Gn1pSt34M48".getBytes());
	public static final String charset = "UTF-8";
	static final Logger _logger = Logger.getLogger(GnipUtil.class.getName());
	private static final String searchCounterURL = "https://search.gnip.com/accounts/iSwarm/search/prod/counts.json";
	static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMddHHmm");
	static final SimpleDateFormat postedTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
	public static final long SEARCH_LIMIT = 5000;
	public static HashMap<String, Long> projectedResultCountByTag = new HashMap<String, Long>();
	private static int API_request_counter = 0;
	private static final String SEARCH_URL = "https://search.gnip.com:443/accounts/iSwarm/search/prod.json"; // old
//	private static final String SEARCH_URL ="https://gnip-api.twitter.com/search/30day/accounts/iSwarm/prod.json"; // new
	
	public static int getAPIRequestCounter() {
		return(API_request_counter);
	}
	
	public static String getAuthHeader() throws UnsupportedEncodingException {
		return(basicAuthToken);
	}
	
	public static HttpURLConnection getGzipConnection(String urlString) throws IOException {
		return(getConnection(urlString, true));
	}
	
	/**
	@deprecated Use getGzipConnection instead.
	*/
	public static HttpURLConnection getConnection(String urlString) throws IOException {
		return(getConnection(urlString, false));
	}
	
	private static HttpURLConnection getConnection(String urlString, boolean acceptGzip) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setReadTimeout(1000 * 60 * 60);
		connection.setConnectTimeout(1000 * 10);
		connection.setRequestProperty("Authorization", GnipUtil.getAuthHeader());
		if (acceptGzip) {
			connection.setRequestProperty("Accept-Encoding", "gzip");
		}
		return connection;
	}
	
	public static void handleNonSuccessResponse(HttpURLConnection connection) throws IOException {
		int responseCode = connection.getResponseCode();
		String responseMessage = connection.getResponseMessage();
		System.err.println("Non-success response: " + responseCode + " -- " + responseMessage);
	}
	
	public static JSONObject search(final String rule, final Date startDate, final Date endDate, boolean useCaches) {
		projectedResultCountByTag = new HashMap<String, Long>();
//		 "fromDate":"<yyyymmddhhmm>"
//		String cachedResultString = (String) cache.get("GnipSearch" + rule.hashCode() + days);
//		String cachedResultString = GNIP_CACHE.get("GnipSearch" + rule.hashCode() + days);
//		String[] cacheNames = _cacheManager.getCacheNames();
//		for (String name : cacheNames) {
//			_logger.info("Found cache with name [" + name + "]");
//		}
		final String key = "GnipSearch" + rule.hashCode() + startDate.hashCode() + endDate.hashCode();
		try {
			final String cachedResultString = jedis.get(key);
			if (cachedResultString != null) {
				_logger.info("**** cache hit!");
				return(new JSONObject(cachedResultString));
			}
		} catch (redis.clients.jedis.exceptions.JedisConnectionException e1) {
			_logger.severe("looks like connection to redis has disappeared");
			e1.printStackTrace();
		}
//		if (useCaches) {
//			// - find a gnipsearch whose start is before startDate and end is after endDate
//			GnipResult gnipResult = DatabaseClient.GnipService.findGnipSearchByRuleSpanningDates(rule, startDate, endDate);
//			if (gnipResult != null) {
//				_logger.info("*************** database hit!!! GnipResult " + gnipResult.getId() + " found");
//				JSONObject results = new JSONObject(gnipResult.getJsonResults());
//				JSONArray resultsArray = results.getJSONArray("results");
//				JSONObject entireResults = new JSONObject();
//				entireResults.put("results", new JSONArray());
//				for (int i = 0;i < resultsArray.length();i++) {
//					// 2014-11-29T17:32:18.000Z
//					try {
//						JSONObject tweet = resultsArray.getJSONObject(i);
//						Date postedTime = postedTimeFormat.parse(tweet.getString("postedTime"));
//						if (postedTime.after(startDate) && postedTime.before(endDate)) {
//							entireResults.append("results", tweet);
//						}
//					} catch (JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (ParseException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//				try {
//					jedis.set(key, entireResults.toString());
//					jedis.expire(key, 4320000);
//				} catch (Exception e) {
//					_logger.severe("problem saving to redis");
//					e.printStackTrace();
//				}
//				return(results);
//			} else {
//				_logger.info("******* database miss for the rule [" + rule + " with start date [" + startDate + "] and end date [" + endDate + "]");
//				try {
//					long projectedResultCount = getNumberOfResultsForRule(rule, SDF.format(startDate.getTime()), SDF.format(endDate.getTime()));
//					if (projectedResultCount == 0) {
//						return(null);
//					}
//					projectedResultCountByTag.put(rule, projectedResultCount);
//				} catch (IOException ingrid) {
//					ingrid.printStackTrace();
//	//				throw new IllegalStateException(ingrid.getMessage());
//				}
//			}
//		}
		String fromDate = SDF.format(startDate.getTime());
		String toDate = SDF.format(endDate.getTime());
		JSONObject entireResults = null;
		int resultsRetrieved = 0;
		HttpURLConnection connection = null;
		InputStream inputStream = null;
		String next = null;
		do {
			try {
				_logger.info("opening a connection to Gnip...");
				connection = GnipUtil.getGzipConnection(SEARCH_URL);
				connection.setDoOutput(true);
				connection.setReadTimeout(10000); // set to 10 seconds per Gnip technical support's recommendation on Apr 3, 2015.
				String query = null;
				long max = (GnipUtil.SEARCH_LIMIT < 500) ? GnipUtil.SEARCH_LIMIT : 500;
				if (next != null) {
					query = String.format("{\"query\":\"%s\",\"publisher\":\"twitter\",\"maxResults\":\"" + max + "\",\"fromDate\":\"" + fromDate + "\",\"toDate\":\"" + toDate + "\",\"next\":\"" + next + "\"}", rule);
//					_logger.info("next token is: " + next);
				} else {
					query = String.format("{\"query\":\"%s\",\"publisher\":\"twitter\",\"maxResults\":\"" + max + "\",\"fromDate\":\"" + fromDate + "\",\"toDate\":\"" + toDate + "\"}", rule);
				}
				_logger.info("query is [" + query + "]");
				OutputStream output = null;
				try {
					connection.connect();
					output = connection.getOutputStream();
					_logger.info("connection has been opened: " + connection.toString());
					_logger.info("writing query to connection...");
					output.write(query.getBytes(GnipUtil.charset));
					output.flush();
					_logger.info("...query has been written to connection.");
				} catch (SocketException sarah) {
					sarah.printStackTrace();
					_logger.info("************ SocketException caught, trying to recover...");
					continue;
				} finally {
					if (output != null) {
						try {
							_logger.info("trying to close outputstream...");
							output.close();
							_logger.info("...outputstream has been closed.");
						} catch (IOException irene) {
							irene.printStackTrace();
						}
					}
				}
				_logger.info("reading response code...");
				int responseCode = -1;
				try {
					responseCode = connection.getResponseCode();
				} catch (java.net.SocketTimeoutException ed) {
					// This probably means Gnip has gotten stuck
					_logger.warning("****** Looks like Gnip got stuck. Trying this request again at " + new Date());
//					ed.printStackTrace();
					continue;
				}
//				_logger.info("response code is " + responseCode);
				if (responseCode >= 200 && responseCode <= 299) {
					inputStream = connection.getInputStream();
					BufferedReader bReader;
					try {
						bReader = new BufferedReader(new InputStreamReader(new StreamingGzipInputStream(inputStream)));
						String line = null;
						do {
//							_logger.info("waiting to read...");
							line = bReader.readLine();
//							_logger.info("...line has been read");
							if (line == null) {
								continue;
							}
	//						_logger.info("trying to parse this into a JSON object [" + line + "]");
							JSONObject json = new JSONObject(line);
							try {
								next = json.getString("next");
							} catch (JSONException e) {
								_logger.info("*** reply did not have a next token");
								next = null;
							}
							org.json.JSONArray results = json.getJSONArray("results");
							int hits = results.length();
							if (entireResults == null) {
								entireResults = new JSONObject();
								entireResults.put("results", new JSONArray());
							} 
							for (int i = 0;i < hits;i++) {
								JSONObject result = results.getJSONObject(i);
	//							if (result.getString("verb").equals("post")) {
									entireResults.append("results", result);
	//							} else {
	//								_logger.info("not including a [" + result.getString("verb") + "]: " + result.getString("body"));
	//							}
							}
							resultsRetrieved += hits;
							_logger.info("results retrieved so far " + resultsRetrieved);
						} while (line != null && (resultsRetrieved < GnipUtil.SEARCH_LIMIT));
					} catch (java.util.zip.ZipException zed) {
						// This happens when a request to Gnip gets botched and then request is somehow not Gzip formatted
						zed.printStackTrace();
						return(entireResults);
					}
				} else {
					_logger.info("an error occurred");
					GnipUtil.handleNonSuccessResponse(connection);
					BufferedReader errorReader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
					String line = errorReader.readLine();
					while (line != null) {
						_logger.info(line);
						line = errorReader.readLine();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				API_request_counter++;
				if (connection != null) {
					connection.disconnect();
				}
			}
		} while (next != null  && (resultsRetrieved < GnipUtil.SEARCH_LIMIT));
//		cache.set("GnipSearch" + rule.hashCode() + days, entireResults.toString(), GNIP_CACHE_SECONDS);
		_logger.info("done reading. resultsRetrieved was " + resultsRetrieved);
		if (entireResults != null) {
			try {
				jedis.set(key, entireResults.toString());
				jedis.expire(key, 4320000);
			} catch (Exception e) {
				_logger.severe("could not write Gnip results to redis");
				e.printStackTrace();
			}
//			if (useCaches) {
//				DatabaseClient db = DatabaseClient.getInstance();
//				db.beginTransaction();
//				// see if the db already contains this GnipSearch by its rule
//				GnipSearch search = DatabaseClient.GnipService.findGnipSearchByRule(rule);
//				if (search == null) {
//					_logger.info("GnipSearch for rule [" + rule + "] was not found.");
//					search = new GnipSearch();
//				} else {
//					_logger.info("GnipSearch for rule [" + rule + "] was found.");
//				}
//				_logger.info("setting the rule on the GnipSearch...");
//				search.setRule(rule);
//				_logger.info("alocating new GnipResult()...");
//				GnipResult result = new GnipResult();
//				_logger.info("settings the start date...");
//				result.setStartTime(startDate);
//				_logger.info("settings the end date...");
//				result.setEndTime(new Date());
//				_logger.info("settings the GnipSearch on the GnipResult...");
//				result.setGnipSearch(search);
//				_logger.info("setting the entire results on the GnipResult object...");
//				result.setJsonResults(entireResults.toString());
//				_logger.info("getting the GnipResults collection from the GnipSearch object...");
//				Collection<GnipResult> results = search.getGnipResults();
//				if (results == null) {
//					_logger.info("GnipSearch with rule [" + rule + "] had no GnipResults");
//					results = new ArrayList<GnipResult>();
//				} else {
//					_logger.info("GnipSearch with rule [" + rule + "] had " + results.size() + " GnipResults");
//				}
//				results.add(result);
//				search.setGnipResults(results);
//				db.merge(search);
//				db.endTransaction();
//			}
		}
		return(entireResults);
	}
	
	public static long getNumberOfResultsForRule(String rule, Date fromDate, Date toDate) throws IOException {
		return(getNumberOfResultsForRule(rule, SDF.format(fromDate), SDF.format(toDate)));
	}
	
	public static long getNumberOfResultsForRule(String rule, String fromDate, String toDate) throws IOException {
		long resultCounter = 0;
		//String rule = "(lang:en OR country_code:us) \\\"cold weather\\\"";
		String query = String.format("{\"query\":\"%s\",\"publisher\":\"twitter\",\"bucket\":\"day\",\"fromDate\":\"" + fromDate + "\",\"toDate\":\"" + toDate + "\"}", rule);
		_logger.info("query is: " + query);
		HttpURLConnection connection = null;
		InputStream inputStream = null;
		try {
			connection = GnipUtil.getConnection(searchCounterURL);
			OutputStream output = null;
			try {
				connection.setDoOutput(true);
				output = connection.getOutputStream();
				output.write(query.getBytes(GnipUtil.charset));
			} finally {
				if (output != null)
					try {
						output.close();
					} catch (IOException logOrIgnore) {
					}
			}
			int responseCode = -1;
			try {
				responseCode = connection.getResponseCode();
			} catch (java.net.SocketException rachel) {
				_logger.warning("could not get a search count from Gnip for the query: " + query + " at " + new Date());
				rachel.printStackTrace();
			}
//			String responseMessage = connection.getResponseMessage();
			if (responseCode >= 200 && responseCode <= 299) {
				inputStream = connection.getInputStream();
				InputStreamReader reader = new InputStreamReader(inputStream);
				BufferedReader bReader = new BufferedReader(reader);
//				String line = bReader.readLine();
//				_logger.info("Response Code: " + responseCode + " -- " + responseMessage);
				String line = null;
				do {
					line = bReader.readLine();
//					_logger.info("line is [" + line + "]");
					if (line == null) {
						break;
					}
					JSONObject json = new JSONObject(line);
					org.json.JSONArray results = json.getJSONArray("results");
					int hits = results.length();
					if (hits > 0) {
						for (int i = 0;i < hits;i++) {
							JSONObject result = results.getJSONObject(i);
							resultCounter += result.getLong("count");
						}
					}
				} while (line != null);
//				_logger.info("exiting");
//				System.err.flush();
//				System.exit(0);
			} else {
				_logger.info("an error occurred");
				GnipUtil.handleNonSuccessResponse(connection);
				inputStream = connection.getErrorStream();
				InputStreamReader reader2 = new InputStreamReader(inputStream);
				BufferedReader bReader2 = new BufferedReader(reader2);
				String line2 = bReader2.readLine();
				while (line2 != null) {
					_logger.info(line2);
					line2 = bReader2.readLine();
				}
			}
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		_logger.info("the rule [" + rule + "] has " + resultCounter + " results");
		return(resultCounter);
	}
	
}
