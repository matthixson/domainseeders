package com.steelgate;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * This class used to run at Spam Arrest, but their attorneys have shut down that arrangement so this no longer runs. booooo.....
 * 
 * -Djmshost=http://server1.spamarrest.com:1856
 * @author mhixson
 * mmm, Spam...
 */
@Deprecated
public class SpamListener extends Thread {
	public static final String HOSTNAME = "cis-lookup.cloudapp.net";
	public static final int PORT = 8080;
//	public static final String HOSTNAME = "www.poindextrose.org";    
//	public static final int PORT = 8181;
	static final Logger _logger = Logger.getLogger(SpamListener.class.getName());
	private QueueSession _qs;
	private QueueConnection _qc;
	private InitialContext _ic;
	private QueueReceiver _qr = null;
	private static int numThreads = 1;
	private static int _messageCounter = 0;
	
	public static void main(String args[]) {
		if (args[0] != null) {
			numThreads = Integer.parseInt(args[0]);
		}
		for (int i = 0; i < numThreads; i++) {
			SpamListener s = new SpamListener();
			s.start();
		}
	}
	
	public void run() {
		_logger.log(Level.INFO, "Starting " + getName());
		try {
			while (true) {
				try {
					_logger.info(getName() + " waiting");
					MapMessage m = (MapMessage) _qr.receive();
					_logger.info(getName() + " received message. message counter: " + ++_messageCounter);
					// TODO: does this also need to replace \r ?
					String message = new String(m.getString("message")).replaceAll("\n", "");
					final String messageAsData = ("data=" + message);
					try {
						_logger.info("opening a new socket to " + HOSTNAME + ":" + PORT);
						HttpURLConnection connection = null;
						URL API_URL = new URL("http://" + HOSTNAME + ":" + PORT + "/drugs/spamSearcher");
						connection = (HttpURLConnection) API_URL.openConnection();
						connection.setConnectTimeout(1000 * 10);
						connection.setRequestMethod("POST");
			      connection.setRequestProperty("Content-Type", "multipart/form-data");
			      connection.setRequestProperty("Content-Length", Integer.toString(messageAsData.getBytes().length));
			      connection.setRequestProperty("Content-Language", "en-US");  
			      connection.setUseCaches(false);
			      connection.setDoInput(true);
			      connection.setDoOutput(true);
						OutputStream output = connection.getOutputStream();
						output.write(messageAsData.getBytes());
						output.flush();
						output.close();
						_logger.info("waiting to read response code...");
						int responseCode = connection.getResponseCode();
						_logger.info("response code was [" + responseCode + "]");
					} catch (IOException e) {
						_logger.info("Problem communicating with SpamSearcher at " + HOSTNAME + ":" + PORT);
						e.printStackTrace();
					}
				} catch (Throwable e) {
					e.printStackTrace();
				} 
			}
		} finally {
			_logger.log(Level.INFO, "Thread [" + getName() + "] has finished running");
			try {
				_logger.info("trying to close the QueueReceiver...");
				_qr.close();
				_logger.info("QueueReceiver closed.");
				_logger.info("trying to close the QueueSession...");
				_qs.close();
				_logger.info("QueueSession closed.");
				_logger.info("trying to close the QueueConnection...");
				_qc.close();
				_logger.info("QueueConnection closed.");
				_logger.info("trying to close the InitialContext...");
				_ic.close();
				_logger.info("InitialContext closed.");
			} catch (JMSException stan) {
				_logger.info("********************* there was an error while closing the JMS objects");
			} catch (NamingException stan) {
				_logger.info("error while closing the InitialContext");
			}
		}
	}
	
	public SpamListener() {
		/** 
		 *  - connect to the JMS server
		 *  - listen to the Steelgate queue
		 *  - when a new message arrives pass that on to Steelgate
		 */
		try {
			_logger.info("Getting Fiorano context...");
			_ic = getFioranoInitialContext();
			_logger.info("Getting queue connection...");
			_qc = getFioranoQueueConnection(_ic);
			_logger.info("Starting queue connection...");
			_qc.start();
			_logger.info("Creating queue session...");
			_qs = _qc.createQueueSession(false, 1);
			_logger.info("Lookup up queue 'steelgate'");
			javax.jms.Queue q = (javax.jms.Queue)_ic.lookup("steelgate");
			_logger.info("Creating the queue receiver...");
			_qr = _qs.createReceiver(q);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	private static QueueConnection getFioranoQueueConnection(InitialContext ic) throws NamingException, JMSException {
		QueueConnection qc = null;
		try {
			_logger.info("Getting the QueueConnectionFactory...");
			QueueConnectionFactory factory = (QueueConnectionFactory) ic.lookup("primaryQCF");
			_logger.info("Creating the queue connection...");
			qc = factory.createQueueConnection();
		} catch (NamingException fred) {
			_logger.warning("You probably need to point jmshost to a Fiorano JMS server.");
			throw(fred);
		}
		return(qc); 
	}
	
	/**
	 * 
	 * @param initialContextUrl
	 * @return
	 * @throws NamingException
	 */
	private static InitialContext getFioranoInitialContext() throws NamingException {
		Properties env = new Properties();
		/* FIORANO */
		env.put(Context.INITIAL_CONTEXT_FACTORY, "fiorano.jms.runtime.naming.FioranoInitialContextFactory");
		env.put(Context.SECURITY_PRINCIPAL, "anonymous");
		env.put(Context.SECURITY_CREDENTIALS, "anonymous");
		// env.put(Context.PROVIDER_URL,"http://m3.spamarrest.com:1856");
		String providerUrl = System.getProperty("jmshost"); // "http://jms01.spamarrest.com:1856";
		env.put(Context.PROVIDER_URL, providerUrl);
		InitialContext ic = new InitialContext(env);
		return(ic); // JNDI connection
	}
	
}
