package com.steelgate.security;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.codec.binary.Base64;
import org.restlet.security.MapVerifier;

public class JiraMapVerifier extends MapVerifier {
	
	@Override
	public int verify(String identifier, char[] secret){
		URL API_URL;
		try {
			API_URL = new URL("https://ceibaintelligence.atlassian.net/rest/api/2/issue/createmeta");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return(MapVerifier.RESULT_UNKNOWN);
		}
		try {
			HttpURLConnection connection = (HttpURLConnection) API_URL.openConnection();
			connection.setRequestProperty("Content-Type", "application/json");
			connection.addRequestProperty("Authorization", "Basic " + Base64.encodeBase64String((identifier + ":" + new String(secret)).getBytes()));
			connection.setConnectTimeout(1000 * 10);
			int responseCode = connection.getResponseCode();
			if (responseCode >= 200 && responseCode <= 299) {
				return(MapVerifier.RESULT_VALID);
			}
			if (responseCode == 401) {
				return(MapVerifier.RESULT_INVALID);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return(MapVerifier.RESULT_MISSING);
		}
		return(MapVerifier.RESULT_UNKNOWN);
	}
	
	public static void main(String[] args) {
		JiraMapVerifier v = new JiraMapVerifier();
		System.err.println("Trying to validate the credentials " + args[0] + ":" + args[1]);
		int result = v.verify(args[0], args[1].toCharArray());  // silly
		if (result == MapVerifier.RESULT_VALID) {
			System.err.println("Success!");
		} else {
			System.err.println("Could not verify credentials.");
		}
	}
}
