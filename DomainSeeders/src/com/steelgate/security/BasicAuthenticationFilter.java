package com.steelgate.security;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.restlet.security.Verifier;

import com.steelgate.servlets.DrugServlet;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;
import java.util.logging.Logger;

/**
 * Created by kemanson on 12/02/14.
 */
public class BasicAuthenticationFilter implements Filter {
	static final Logger _logger = Logger.getLogger(DrugServlet.class.getName());

  private String realm = "Protected";

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    String paramRealm = filterConfig.getInitParameter("realm");
    if (StringUtils.isNotBlank(paramRealm)) {
      realm = paramRealm;
    }
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) servletRequest;
    HttpServletResponse response = (HttpServletResponse) servletResponse;
    JiraMapVerifier verifier = new JiraMapVerifier();
    // see if we have a token cached in the user's session first
    HttpSession session = request.getSession();
    Boolean loggedIn = (Boolean) session.getAttribute("loggedIn");
    if (loggedIn != null && loggedIn) {
    	_logger.info("user already logged in, not asking for username/password");
    	filterChain.doFilter(servletRequest, servletResponse);
    	return;
    }
    String authHeader = request.getHeader("Authorization");
    if (authHeader != null) {
      StringTokenizer st = new StringTokenizer(authHeader);
      if (st.hasMoreTokens()) {
        String basic = st.nextToken();
        if (basic.equalsIgnoreCase("Basic")) {
          try {
            String credentials = new String(Base64.decodeBase64(st.nextToken()), "UTF-8");
//            _logger.info("Credentials: " + credentials);
            int p = credentials.indexOf(":");
            if (p != -1) {
              String username = credentials.substring(0, p).trim();
              String password = credentials.substring(p + 1).trim();
              int result = verifier.verify(username, password.toCharArray());
              if (result == Verifier.RESULT_VALID) {
              	_logger.info("setting loggedIn to true in the session of user [" + username + "]");
              	session.setAttribute("loggedIn", true);
              	filterChain.doFilter(servletRequest, servletResponse);
              } else {
              	unauthorized(response, "Bad credentials");
              }
            } else {
              unauthorized(response, "Invalid authentication token");
            }
          } catch (UnsupportedEncodingException e) {
            throw new Error("Couldn't retrieve authentication", e);
          }
        }
      }
    } else {
      unauthorized(response);
    }
  }

  @Override
  public void destroy() {
  }

  private void unauthorized(HttpServletResponse response, String message) throws IOException {
    response.setHeader("WWW-Authenticate", "Basic realm=\"" + realm + "\"");
    response.sendError(401, message);
  }

  private void unauthorized(HttpServletResponse response) throws IOException {
    unauthorized(response, "Unauthorized");
  }

}
