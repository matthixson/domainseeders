package com.steelgate.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries ({
	@NamedQuery(name = "findManufacturerByName",
  	  query = "SELECT m FROM Manufacturer m WHERE m.name = :name")
})
@Entity
@Table(name = "manufacturers")
public class Manufacturer implements Serializable, Comparable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
  @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long _id;
	@Column(name="name")
	private String name;
	
	public Manufacturer() {
		
	}
	
	public Manufacturer(String name) {
		this.name = name;
	}

	public long getId() {
		return _id;
	}
	
	public void setId(long id) {
		_id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Object o) {
		Manufacturer other = (Manufacturer) o;
		return(name.compareTo(other.getName()));
	}
	
}
