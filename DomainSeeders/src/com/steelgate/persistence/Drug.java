package com.steelgate.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Logger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
/**
 * 
   alter table drugs add column spam boolean not null default true;
   alter table drugs add column twitter boolean not null default false;
   alter table drugs add column domain boolean not null default false;
   alter table drugs add column indexed_web boolean not null default true;
   alter table drugs add column facebook boolean not null default true;
 * 
 * @author mhixson
 *
 */
@NamedQueries ({
	@NamedQuery(name = "findDrugByName",
  	  query = "SELECT d FROM Drug d WHERE d.name = :name"),
  @NamedQuery(name = "findDrugStartingWith",
  	  query = "SELECT d FROM Drug d WHERE d.name like :name order by d.name")
})
@Entity
@Table(name = "drugs")
public class Drug implements Serializable, Comparable {
	@Id
  @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long _id;
	@Column(name="name")
	private String name;
	@OneToMany(fetch = FetchType.LAZY,cascade=CascadeType.ALL)
  @JoinColumn(name="drug_id", referencedColumnName="id")
	private Collection<Variation> variations;
	static final Logger _logger = Logger.getLogger(Drug.class.getName());
	private boolean twitter;
	private boolean spam;
	private boolean domain;
	private boolean indexed_web;
	private boolean facebook;
	
	public Drug() {
	}
	
	public Drug(String drug) {
		this.name = drug;
	}

	public long getId() {
		return _id;
	}
	
	public void setId(long id) {
		_id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public Collection<Variation> getVariations() {
		return variations;
	}

	public void setVariations(Collection<Variation> variations) {
		this.variations = variations;
	}
	
	public String toString() {
		return("[" + _id + "] " + name);
	}
	
	public boolean isTwitter() {
		return twitter;
	}
	public void setTwitter(boolean twitter) {
		this.twitter = twitter;
	}
	public boolean isSpam() {
		return spam;
	}
	public void setSpam(boolean spam) {
		this.spam = spam;
	}
	public boolean isDomain() {
		return domain;
	}
	public void setDomain(boolean domain) {
		this.domain = domain;
	}
	public boolean isIndexedWeb() {
		return indexed_web;
	}
	public void setIndexedWeb(boolean indexedWeb) {
		this.indexed_web = indexedWeb;
	}
	public boolean isFacebook() {
		return facebook;
	}
	public void setFacebook(boolean facebook) {
		this.facebook = facebook;
	}

	@Override
	public int compareTo(Object o) {
		Drug otherDrug = (Drug) o;
		return(name.compareTo(otherDrug.getName()));
	}
	
}
