package com.steelgate.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * 
 * @author mhixson
 * 
 */
public class SteelgateDatabaseClient {
	protected static EntityManagerFactory factory = Persistence.createEntityManagerFactory("steelgatePU");
	protected EntityManager em;
	static Logger _logger = Logger.getLogger(SteelgateDatabaseClient.class.getName());

	private SteelgateDatabaseClient() {
		try {
			if (em == null) {
				em = factory.createEntityManager();
			}
		} catch (Exception ex) {
			_logger.severe(ex.getMessage());
		}
	}

	public EntityManager getEntityManager() {
		return em;
	}

	public static SteelgateDatabaseClient getInstance() {
		return new SteelgateDatabaseClient();
	}

	public void beginTransaction() {
		em.getTransaction().begin();
	}

	public void endTransaction() {
		try {
			em.getTransaction().commit();
		} catch (/*
							 * RollbackException | HeuristicMixedException |
							 * HeuristicRollbackException |
							 */SecurityException | IllegalStateException /* | SystemException */ex) {
			_logger.severe(ex.getMessage());
		}
	}

	public Map<String, Long> getDomainSourcesStatsBetweenDates(Date start, Date end) {
		Query query = em.createNativeQuery("select search_tool, count(search_tool) as count from domain_seeds where search_tool is not null and date_added between '" + start + "' AND '" + end + "' group by search_tool");
		List<Object[]> resultList = query.getResultList();
		Map<String, Long> resultMap = new TreeMap<String, Long>();
		for (Object[] result : resultList) {
			resultMap.put((String)result[0], (Long)result[1]);
		}
		return(resultMap);
	}
	
	public <T> T find(Class<T> entityClass, Object primaryKey, Object value) {
		if (value instanceof String) {
			value = Long.parseLong((String) value);
		} else if (value instanceof Long) {
			// value = (Long) value;
		}
		return em.find(entityClass, primaryKey);
	}

	public <T> T findSingleResult(Class<T> entityClass, Object primaryKey) {
		return em.find(entityClass, primaryKey);
	}

	public <T> List<T> findAll(Class<T> entityClass) {
		String query = "SELECT e FROM " + entityClass.getSimpleName() + " e ";
		List<T> results = em.createQuery(query).getResultList();
		return results;
	}

	public <T> T merge(T entity) {
		// System.out.println("Merging: " + entity);
		T result = null;
		try {
			result = em.merge(entity);
			em.flush();
		} catch (Exception ex) {
			try {
				_logger.warning("rolling back because of: " + ex.getMessage());
				em.getTransaction().rollback();
				// utx.rollback();
			} catch (Exception e) {
				_logger.severe(ex.getMessage());
			}
			_logger.severe(ex.getMessage());
		}
		return result;
	}

	public Collection mergeAll(Collection objs) {
		ArrayList results = new ArrayList();
		try {
			for (Object obj : objs) {
				results.add(em.merge(obj));
			}
		} catch (Exception ex) {
			try {
				_logger.warning("rolling back because of: " + ex.getMessage());
				em.getTransaction().rollback();
				// utx.rollback();
			} catch (Exception e) {
				_logger.severe(ex.getMessage());
			}
			_logger.severe(ex.getMessage());
		}
		return results;
	}

	public void remove(Object entity) {
		em.remove(em.merge(entity));
	}

	public void delete(Object entity) {
		_logger.info("deleting object [" + entity + "]");
		em.remove(entity);
	}

	public void persist(Object entity) {
		_logger.info("persisting object [" + entity + "]");
		em.persist(entity);
	}

	public Manufacturer findManufacturerByName(String name) {
		Manufacturer m = (Manufacturer) getInstance().em.createNamedQuery("findManufacturerByName").setParameter("name", name).setMaxResults(1).getSingleResult();
		return(m);
	}

	public Drug findDrugByName(String name) {
		Drug drug = null;
		try {
			drug = (Drug) getInstance().em.createNamedQuery("findDrugByName").setParameter("name", name).setMaxResults(1).getSingleResult();
		} catch (javax.persistence.NoResultException e) {
			_logger.warning("no drug found with the name [" + name + "]");
			e.printStackTrace();
		}
		return(drug);
	}

	public List<Drug> findDrugsStartingWith(String startsWith) {
		List<Drug> drugs = (List<Drug>) getInstance().em.createNamedQuery("findDrugStartingWith").setParameter("name", startsWith + "%").getResultList();
		return(drugs);
	}

	public List<Drug> findDrugsForSource(String source) {
		final List<Drug> drugs = findAll(Drug.class);
		List<Drug> tempList = new ArrayList<Drug>();
		Collections.sort(drugs);
		switch (source) {
			case "twitter":
				for (Drug drug : drugs) {
					if (drug.isTwitter()) {
						tempList.add(drug);
					}
				}
				break;
			case "facebook":
				for (Drug drug : drugs) {
					if (drug.isFacebook()) {
						tempList.add(drug);
					}
				}
				break;
			case "domain":
				for (Drug drug : drugs) {
					if (drug.isDomain()) {
						tempList.add(drug);
					}
				}
				break;
			case "indexed_web":
				for (Drug drug : drugs) {
					if (drug.isIndexedWeb()) {
						tempList.add(drug);
					}
				}
				break;
			case "spam":
				for (Drug drug : drugs) {
					if (drug.isSpam()) {
						tempList.add(drug);
					}
				}
				break;
		}
		return(tempList);
	}

	/**
	 * Runs this SQL:
	 * 
	 * select drug_id from variations where manufacturer_id in (select id from
	 * manufacturers where name = 'Merck');
	 * 
	 * @param m
	 * @return
	 */
	public List<Drug> getDrugsForManufacturer(Manufacturer m) {
		Query q = getInstance().em.createNativeQuery("select drug_id from variations where manufacturer_id in (select id from manufacturers where name = '" + m.getName() + "')");
		List<Long> drugIds = q.getResultList();
		// _logger.info("the drug id list is: " + drugIds);
		List<Drug> drugs = new ArrayList<Drug>();
		for (Long id : drugIds) {
			Drug d = this.findSingleResult(Drug.class, id);
			drugs.add(d);
		}
		return(drugs);
	}

	public List<DomainSeed> findDomainSeedsBetweenDates(Date start, Date end) {
		List<DomainSeed> seeds = em.createNamedQuery("findDomainSeedsBetweenDates").setParameter("start", start).setParameter("end", end).getResultList();
		return(seeds);
	}
}
