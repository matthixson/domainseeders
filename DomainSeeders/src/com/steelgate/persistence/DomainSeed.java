package com.steelgate.persistence;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@NamedQueries ({
	@NamedQuery(name = "findDomainSeedsBetweenDates",
  	  query = "SELECT d FROM DomainSeed d WHERE d.dateAdded BETWEEN :start AND :end")
})
@Entity
@Table(name = "domain_seeds")
public class DomainSeed implements Serializable, Comparable {
	private static final long serialVersionUID = 1L;
    @Id    
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
	private long _id;
    @Column(name="name")
	private String _name;
    @Column(name="description")
	private String _description;
    @Column(name = "search_tool")
	private String _searchTool;
    @Column(name = "url")
	private String _url;
    @Column(name = "date_added")
    @Temporal(TemporalType.TIMESTAMP)
	private Date dateAdded;
	
	public long getId() {
		return _id;
	}
	public void setId(long id) {
		_id = id;
	}
	public String getName() {
		return _name;
	}
	public void setName(String name) {
		_name = name;
	}
	public String getDescription() {
		return _description;
	}
	public void setDescription(String description) {
		_description = description;
	}
	
	public String getSearchTool() {
		return _searchTool;
	}
	public void setSearchTool(String searchTool) {
		_searchTool = searchTool;
	}
	public String getUrl() {
		return _url;
	}
	public void setUrl(String url) {
		_url = url;
	}
	
	
	public Date getDateAdded() {
		return dateAdded;
	}
	public void setDateAdded(Date timestamp) {
		dateAdded = timestamp;
	}
	@Override
	public int compareTo(Object o) {
		return(this.getName().compareTo(((DomainSeed)o).getName()));
	}
	
}
