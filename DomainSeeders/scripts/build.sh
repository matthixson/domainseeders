#!/bin/sh
/usr/local/tomcat/bin/shutdown.sh
bin=`pwd`
start=`date "+%s"`
rm -rf /Volumes/buildspace/*
tmp=`mktemp -d "/Volumes/buildspace/XXXXXXXXXX"`
echo "should have just made directory $tmp"
mkdir $tmp/build
tmpdir=$tmp/build
echo "Building into $tmpdir"
echo "Copying class files"
mkdir -p $tmpdir/WEB-INF/classes
cp -r ../bin/* $tmpdir/WEB-INF/classes
cp -r ../WEB-INF/* $tmpdir/WEB-INF
mv $tmpdir/WEB-INF/*.properties $tmpdir/WEB-INF/classes
echo "Copying all libraries"
mkdir $tmpdir/WEB-INF/lib
cp `find ../lib -name "*.jar"` $tmpdir/WEB-INF/lib
echo "Copying META-INF dir into WEB-INF/classes"
cp -r ../META-INF $tmpdir/WEB-INF/classes
cd $tmpdir
# TODO: cleanup git detritus 
#echo "Cleaning up subversion detritus"
#rm -rf `find . -name ".svn"`
rm -rf /usr/local/tomcat/webapps/drugs
echo "Creating /usr/local/tomcat/webapps/drugs.war"
rm /usr/local/tomcat/webapps/drugs.war
jar cf /usr/local/tomcat/webapps/drugs.war *
rm -r $tmp
#echo restarting memcached
#ps | grep memcached
#killall -TERM memcached
#ps | grep memcached
#memcached -I 25m &
cd $bin
rm -rf /tmp/tomcat.pid
/usr/local/tomcat/bin/startup.sh
echo "Waiting for Tomcat to finish startup..."
tail -f /usr/local/tomcat/logs/catalina.out | while read LOGLINE
do
   [[ "${LOGLINE}" == *"Server startup"* ]] && pkill -P $$ tail
done
end=`date "+%s"`
TIME=`expr $end - $start`
TIME="$TIME seconds"
echo $TIME
say $TIME