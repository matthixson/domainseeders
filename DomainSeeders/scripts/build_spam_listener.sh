#!/bin/sh
cd /tmp
mkdir -p spam/com/steelgate
mkdir spam/lib
cp ~/git/domainseeders/DomainSeeders/lib/fmq70-* spam/lib
cp ~/git/domainseeders/DomainSeeders/bin/com/steelgate/SpamListener.class spam/com/steelgate
cd spam
jar cvf /tmp/SpamListener.jar lib com